
CFLAGS=-g -Wall -Wextra $(shell pkg-config --cflags glibmm-2.4) -std=c++17
CFLAGS+=$(shell pkg-config --cflags giomm-2.4)
LDFLAGS=$(shell pkg-config --libs glibmm-2.4)
LDFLAGS+=$(shell pkg-config --libs giomm-2.4)

SRC=SocketHandler.cpp main.cpp
OBJ=$(SRC:.cpp=.o)

PRJ=test

$(PRJ):	$(OBJ)
	$(CXX) $(LDFLAGS) -o $@ $^ $(LDFLAGS)

%.o:	%.cpp
	$(CXX) -o $@ -c $< $(CFLAGS)

clean:	
	rm *.o $(PRJ)
