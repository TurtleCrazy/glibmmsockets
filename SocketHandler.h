#ifndef SocketHandler_H
#define SocketHandler_H

#include <glibmm.h>
#include <giomm.h>


class SocketHandler:public Gio::SocketClient
{
	public:
		enum states
		{
			CONNECTING,
			READY,
			ERROR,
			SHUTDOWN
		};
		virtual ~SocketHandler();
		SocketHandler(const Glib::ustring& host,int port);
		void setPatrolCounter(ssize_t counter);
		states status() const;
		bool toBeDestroyed() const;
	protected:

		virtual bool write_handler(Glib::IOCondition io_condition)=0;
		virtual bool read_handler(Glib::IOCondition io_condition)=0;
		virtual bool error_handler(Glib::IOCondition io_condition);
		virtual void on_connect()=0;
		states m_status;
		Glib::RefPtr< Gio::SocketConnection > m_socketclient;
		void selfDestroy();

		friend class SocketHandlerFactory;
		void  patrol();

	private:
		virtual void onPatrol();
		void on_connect_async_ready(Glib::RefPtr<Gio::AsyncResult>& result);
		bool m_garbage;
		ssize_t m_patrol;

};

class SingleRequestSocket final:public SocketHandler
{
	public:
		SingleRequestSocket(const Glib::ustring& host,int port);
		~SingleRequestSocket() =default ;
	protected:
		bool write_handler(Glib::IOCondition io_condition);
		bool read_handler(Glib::IOCondition io_condition);
		void on_connect();
	private:
		void onPatrol();
};


class SocketHandlerFactory
{
	public:
		SocketHandlerFactory(const SocketHandlerFactory&) = delete;
		SocketHandlerFactory& operator=(const SocketHandlerFactory&) = delete;
		static SocketHandlerFactory* get();
		~SocketHandlerFactory()=default;
		void push(std::unique_ptr<SocketHandler>& source);
		void clean();
	private:
		SocketHandlerFactory() = default;
		std::list<std::unique_ptr<SocketHandler> > m_queue;
		static SocketHandlerFactory* m_instance;
		bool patrol();
};

#endif
