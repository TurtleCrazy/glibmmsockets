#include <glibmm.h>
#include <giomm.h>
#include <list>
#include <iostream>
#include <cassert>
#include "SocketHandler.h"

Glib::RefPtr<Glib::MainLoop> loop;

using namespace Glib;
using namespace Gio;

void go()
{
	std::unique_ptr<SocketHandler> s=std::make_unique<SingleRequestSocket>("127.0.0.1",5080);
	s->setPatrolCounter(5);
	SocketHandlerFactory::get()->push(s);
}

class client
{
	public:
		client(Glib::RefPtr<Gio::Socket>& l){
			me=l->accept();
			std::cout << "new client" << std::endl;
			Gio::signal_socket().connect(sigc::mem_fun(this,&client::i_handler), me, Glib::IO_IN | Glib::IO_OUT);
			Gio::signal_socket().connect(sigc::mem_fun(this,&client::o_handler), me, Glib::IO_HUP | Glib::IO_ERR);
		};

		~client(){};
		bool i_handler(Glib::IOCondition io_condition)
		{
			s=me->receive(buf,1023);
			buf[s+1]='\0';
			std::cout << buf;
			me->send("$",2);
			return true;
		}
		bool o_handler(Glib::IOCondition io_condition)
		{
			std::cout << "kill me" << std::endl;
			return false;
		}
	private:
		Glib::RefPtr<Gio::Socket> me;
		char buf[1024];
		gssize s;
};
Glib::RefPtr<Gio::Socket> socket_l;

std::list<client> queue;
bool io_handler(Glib::IOCondition io_condition)
{
	queue.emplace_back(socket_l);
	return true;
}

int main()
{
	Glib::init();
	Gio::init();
	setlocale(LC_ALL, "C");
	std::cout << "local address" << std::endl;

	auto inetAddr=Gio::InetAddress::create 	("127.0.0.1");
	auto  inetsocket=Gio::InetSocketAddress::create(inetAddr,5012);
	std::cout << "socket" << std::endl;
	socket_l=Gio::Socket::create(Gio::SOCKET_FAMILY_IPV4,Gio::SOCKET_TYPE_STREAM
			,Gio::SOCKET_PROTOCOL_TCP)  ;

	std::cout << "binding to address" << std::endl;
	socket_l->bind(inetsocket,true);
	socket_l->listen();


	loop = Glib::MainLoop::create();
	Gio::signal_socket().connect(sigc::ptr_fun(& io_handler), socket_l, Glib::IO_IN | Glib::IO_OUT);
	Glib::signal_timeout().connect_seconds_once(sigc::ptr_fun(&go),5);
	loop->run();

}
